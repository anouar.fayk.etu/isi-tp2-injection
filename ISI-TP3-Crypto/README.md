## TP3 ISI-Cryptographie

## Binomes 
## FAYK Anouar

## TERMECHE Adel

### Question 1:

Afin d'assurer le chiffrement a deux facteurs (presence des deux responsables) pour notre fichier de données bancaire on va proceder de la maniere suivante:

- Géneration d'une cle aleatoire dans ramdisk pour chaque responsable.

- A l'aide de cette cle géenerer on va chiffrer le mot de passe de responsable qui va etre stocke dans usb

- On va generer une cle maitraisse avec laquelle on va chiffre les données bancaires. Cette clé maitraisse est le resultat du xor entre les clé contenu dans l'usb des deux responsables.

- Pour le dechiffrement on a besoin obligatoirement des deux responsable afin d'assurer le decryptage du fichier.

### Qestion 2 : Proof of Concept
**Implementé.**


## Qestion 3 :

- Afin que les representants puissent se substituer aux responsables en cas de besoin lors de la remise en service du serveur, on va suivre les étapes suivantes:
  * ´NB:´ On notera les  deux responsables par ***A1*** et ***B1***, et les deux representants par ***A2*** et ***B2***.
 - Comme il s'agit maintenant d'ajouter une relation 'OU' à la question précedente (il faut que (A1 OU A2 soient présent) ET (B1 OU B2 soient présent)), on va commencer par générer deux clés aléatoires dans la ramdisk, qu'on notera **KeyA** et **KeyB**.
 - Ensuite, on va chiffrer chacune de ces clés générer par les mot de passes des deux utilisateurs (responsable et representant) et les placer dans leurs clé USB. Pour pouvoir déchiffrer ensuite le fichier chiffré par la clé de A ou A' et de B ou B'.
 - On va generer une cle maitraisse avec laquelle on va chiffre les données bancaires. Cette clé maitraisse est le resultat du xor entre les clés contenu dans la ramdisk des deux types d'utilisateurs(responsable ou representant).
- Maintenant, que ce soit l'utilisateur A1 ou A2, et B1 ou B2 qui sont présent, ou pourra déchiffrer leurs fichier facilement.
**usbA1 va contenir la deux cle celle du responsable et son representant**
**usbB1 va contenir la deux cle celle du responsable et son representant**


### Qestion 4 : Proof of Concept
**Implementé.**


### Question 5 :
- L'idée  est de supprimer de la clé USB de l'utilisateur le fichier de l'utilisateur retiré. Ensuite, on pourra simplement déchiffrer les clés des utilisateur qui restent par leurs mot de passe et rien ne va changer.
