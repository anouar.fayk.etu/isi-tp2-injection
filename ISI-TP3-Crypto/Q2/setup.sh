#! /bash/bin


#Creation des dossier utilies pour la mise en place du service
mkdir "usb1"
mkdir "usb2"
mkdir "ramdisk"

#fichier des données bancaires
touch "data"

#Ajout de certains pairs automatiquement
echo "adel 5485" >> data
echo "anouar 254652" >> data


# Responsable 1
dd if=/dev/urandom bs=16 count=1 of=ramdisk/key1
echo "Responsable 1 Veuillez choisir un mot de passe [! Longueur pair !]"
read mdpRes1
#le mdp du resposable 1 va etre chiffre avec la cle key1 genere aleatoirement et va etre stocké dans sa clé usb1
openssl enc -aes-128-cbc -in ./ramdisk/key1 -out ./usb1/key1.enc -k $(echo $mdpRes1 | base64 -d | hexdump -v -e '1/1 "%02x" ') -iter 100
#$(v.bin to hexa) hexdump -ve '1/1 "%02x"'


# Responsable 2
dd if=/dev/urandom bs=16 count=1 of=ramdisk/key2
echo "Responsable 2 Veuillez choisir un mot de passe [! Longueur pair !] "
read mdpRes2
#le mdp du resposable 1 va etre chiffre avec la cle key1 genere aleatoirement et va etre stocké dans sa clé usb2
openssl enc -aes-128-cbc -in ./ramdisk/key2 -out ./usb2/key2.enc -k $(echo $mdpRes2 | base64 -d | hexdump -v -e '1/1 "%02x" ') -iter 100

#Chiffrement des données en claire en suivant la procedure suivante
# On a les deux clé chiffré des deux responsable pour assurer la presence des deux responsable pour le decryptage de notre fichier bancaire
# On va generer une cleMaitraisse qui est le resultat des deux cle chiffre des deux responsable et on va chiffrer data via cette cle maitresse
# chiffrement a deux facteurs 
hash1=$(cat ./usb1/key1.enc | hexdump -v -e '1/1 "%02x"')
hash2=$(cat ./usb2/key2.enc | hexdump -v -e '1/1 "%02x"')
cleMaitraisse=`echo $((0x$hash1 ^ 0x$hash2)) `


openssl enc -aes-128-cbc -k $cleMaitraisse -in data -out data.enc -iter 100 | base64

#supression des données en clair
rm -f data

echo "Mise en service Succes"

