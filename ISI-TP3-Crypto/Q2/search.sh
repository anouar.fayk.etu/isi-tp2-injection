#! /bash/bin

echo "mode recherche "

echo "veuillez entrer le nom du proprietaire de la carte a chercher"
read nom



echo "Entrez le premier mot de passe:"
read mdpRes1
mdp1=$(echo $mdpRes1 | base64 -d | hexdump -v -e '1/1 "%02x" ')
openssl enc -d -aes-128-cbc -in ./usb1/key1.enc -out ./ramdisk/key1 -k $mdp1 -iter 100

echo "Entrez le second mot de passe:"
read mdpRes2
mdp2=$(echo $mdpRes2 | base64 -d | hexdump -v -e '1/1 "%02x" ')

openssl enc -d -aes-128-cbc -in ./usb2/key2.enc -out ./ramdisk/key2 -k $mdp2 -iter 100

hash1=$(cat ./usb1/key1.enc | hexdump -v -e '1/1 "%02x"')
hash2=$(cat ./usb2/key2.enc | hexdump -v -e '1/1 "%02x"')
cleMaitraisse=`echo $((0x$hash1 ^ 0x$hash2)) `

openssl enc -d -aes-128-cbc -in data.enc -out data -k $cleMaitraisse -iter 100 | base64


if grep -q "$nom " data; then
    echo " ## Found ##" && grep "$nom " data
else
    echo "## Not Found ##"
fi

openssl enc -aes-128-cbc -k $cleMaitraisse -in data -out data.enc -iter 100 | base64

#supression des données en clair
rm -f data










