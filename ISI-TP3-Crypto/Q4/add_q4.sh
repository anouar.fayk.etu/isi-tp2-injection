#! /bash/bin

echo "mode ajout nom et num carte dans la base"

echo "veuillez entrer le nom du proprietaire de la carte"
read nom 

echo "veuillez entrer le numero de la carte"
read numero

echo "Si vous êtes le responsable 1 entrer 'A1' et si vous êtes son remplacant entrer 'A2'"
read status1
echo "Entrez le premier mot de passe:"
read mdpA
mdp1=$(echo $mdpA | base64 -d | hexdump -v -e '1/1 "%02x" ')

echo "Si vous êtes le responsable 2 entrer 'B1' et si vous êtes son remplacant entrer 'B2'"
read status2
echo "Entrez le second mot de passe:"
read mdpB
mdp2=$(echo $mdpB | base64 -d | hexdump -v -e '1/1 "%02x" ')

### On aurra 4 possibilités

# on verifie su la 1 ere authentification est faire par le responsable ou son representant
# [A1 ==> B1] [A1 ==> B2] [A2==>B1] [A2==>B2]
if [ $status1 = "A1" ] && [ $status2 = "B1" ]
then
	openssl enc -d -aes-128-cbc -in ./usbA1/keyA1.enc -out ./ramdisk/keyA1 -k $mdp1 -iter 100
	openssl enc -d -aes-128-cbc -in ./usbB1/keyB1.enc -out ./ramdisk/keyB1 -k $mdp2 -iter 100


elif [ $status1 = "A1" ] && [ $status2 = "B2" ]
then
	openssl enc -d -aes-128-cbc -in ./usbA1/keyA1.enc -out ./ramdisk/keyA1 -k $mdp1 -iter 100
	openssl enc -d -aes-128-cbc -in ./usbB1/keyB2.enc -out ./ramdisk/keyB1 -k $mdp2 -iter 100

elif [ $status1 = "A2" ] && [ $status2 = "B1" ]
then
	openssl enc -d -aes-128-cbc -in ./usbA1/keyA2.enc -out ./ramdisk/keyA1 -k $mdp1 -iter 100
	openssl enc -d -aes-128-cbc -in ./usbB1/keyB1.enc -out ./ramdisk/keyB1 -k $mdp2 -iter 100

elif [ $status1 = "A2" ] && [ $status2 = "B2" ]
then
	openssl enc -d -aes-128-cbc -in ./usbA1/keyA2.enc -out ./ramdisk/keyA1 -k $mdp1 -iter 100
	openssl enc -d -aes-128-cbc -in ./usbB1/keyB2.enc -out ./ramdisk/keyB1 -k $mdp2 -iter 100

fi


hash1=$(cat ./ramdisk/keyA1 | hexdump -v -e '1/1 "%02x"')
hash2=$(cat ./ramdisk/keyB1 | hexdump -v -e '1/1 "%02x"')
cleMaitraisse=`echo $((0x$hash1 ^ 0x$hash2)) `


openssl enc -d -aes-128-cbc -in data.enc -out data -k $cleMaitraisse -iter 100 | base64


echo $nom $numero >> data

openssl enc -aes-128-cbc -k $cleMaitraisse -in data -out data.enc -iter 100 | base64

#supression des données en clair
rm -f data

echo "Added Succefully"
