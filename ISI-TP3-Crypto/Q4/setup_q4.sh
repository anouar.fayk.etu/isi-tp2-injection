#! /bash/bin


#Creation des dossier utilies pour la mise en place du service: A1 et B1 pour les responsables et A2 et B2 pour les representants
mkdir "usbA1"
#mkdir "usbA2"
mkdir "usbB1"
#mkdir "usbB2"
mkdir "ramdisk"

#fichier des données bancaires
touch "data"

#Ajout de certains pairs automatiquement
echo "Adel 1234" >> data
echo "Anouar 9876" >> data


# Responsable 1
dd if=/dev/urandom bs=16 count=1 of=ramdisk/keyA1
echo "Responsable 1 Veuillez choisir un mot de passe [! Longueur pair !]"
read mdpRes1
mdp1=$(echo $mdpRes1 | base64 -d | hexdump -v -e '1/1 "%02x" ')

#le mdp du resposable 1 va etre chiffre avec la cle key1 genere aleatoirement et va etre stocké dans sa clé usbA1
openssl enc -aes-128-cbc -in ./ramdisk/keyA1 -out ./usbA1/keyA1.enc -k $mdp1 -iter 100
#$(v.bin to hexa) hexdump -ve '1/1 "%02x"'

# Representant 1
#dd if=/dev/urandom bs=16 count=1 of=ramdisk/keyA2
echo "Representant 1 Veuillez choisir un mot de passe [! Longueur pair !]"
read mdpRep1
mdp2=$(echo $mdpRep1 | base64 -d | hexdump -v -e '1/1 "%02x" ')
#le mdp du resposable 1 va etre chiffre avec la cle key1 genere aleatoirement et va etre stocké dans sa clé usbA1
openssl enc -aes-128-cbc -in ./ramdisk/keyA1 -out ./usbA1/keyA2.enc -k $mdp2 -iter 100
#$(v.bin to hexa) hexdump -ve '1/1 "%02x"'


# Responsable 2
dd if=/dev/urandom bs=16 count=1 of=ramdisk/keyB1
echo "Responsable 2 Veuillez choisir un mot de passe [! Longueur pair !] "
read mdpRes2
mdp3=$(echo $mdpRes2 | base64 -d | hexdump -v -e '1/1 "%02x" ')
#le mdp du resposable 1 va etre chiffre avec la cle key1 genere aleatoirement et va etre stocké dans sa clé usbB1
openssl enc -aes-128-cbc -in ./ramdisk/keyB1 -out ./usbB1/keyB1.enc -k $mdp3 -iter 100

# Representant 2
#dd if=/dev/urandom bs=16 count=1 of=ramdisk/keyB2
echo "Representant 2 Veuillez choisir un mot de passe [! Longueur pair !] "
read mdpRep2
mdp4=$(echo $mdpRep2 | base64 -d | hexdump -v -e '1/1 "%02x" ')
#le mdp du resposable 1 va etre chiffre avec la cle key1 genere aleatoirement et va etre stocké dans sa clé usbB1
openssl enc -aes-128-cbc -in ./ramdisk/keyB1 -out ./usbB1/keyB2.enc -k $mdp4 -iter 100

#Chiffrement des données en claire en suivant la procedure suivante
# On a les deux clé chiffré des deux responsable pour assurer la presence des deux responsable pour le decryptage de notre fichier bancaire
# On va generer une quatres cleMaitraisse qui sont le resultat des deux cle chiffre des (Resp1 et Resp2), (Resp1 et Repres2), (Repres1 et Resp2), (Repres1, Repres2)
 
hash1=$(cat ./ramdisk/keyA1 | hexdump -v -e '1/1 "%02x"')
hash2=$(cat ./ramdisk/keyB1 | hexdump -v -e '1/1 "%02x"')
cleMaitraisse=`echo $((0x$hash1 ^ 0x$hash2)) `


openssl enc -aes-128-cbc -k $cleMaitraisse -in data -out data.enc -iter 100 | base64

#supression des données en clair
rm -f data

echo "Mise en service Succes"
